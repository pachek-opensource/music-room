import dotenv
import uvicorn

dotenv.load_dotenv(".env", override=True)


if __name__ == "__main__":
    uvicorn.run("app.adapters.api.app:create_fastapi", factory=True, reload=True)
