import dotenv
import pytest

dotenv.load_dotenv(".env")


pytest_plugins = [
    "anyio",
    "sqlalchemy_pytest.database",
    "tests.plugins.sqla",
    "tests.plugins.api",
    "tests.plugins.container",
]


@pytest.fixture(scope="session")
def anyio_backend() -> str:
    return "asyncio"
