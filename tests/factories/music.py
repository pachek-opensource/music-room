from polyfactory.factories.sqlalchemy_factory import SQLAlchemyFactory

from app.db.models import Music


class MusicFactory(SQLAlchemyFactory[Music]): ...
