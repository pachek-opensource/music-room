import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

pytestmark = [pytest.mark.anyio]


async def test_ok(session: AsyncSession) -> None:
    stmt = select(1)
    assert await session.scalar(stmt) == 1
