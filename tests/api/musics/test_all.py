import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from tests.factories.music import MusicFactory

pytestmark = [pytest.mark.anyio]


async def test_get_all_musics(
    app: FastAPI,
    http_client: AsyncClient,
    session: AsyncSession,
) -> None:
    musics = MusicFactory.batch(size=10)
    session.add_all(musics)
    await session.flush()

    response = await http_client.get(app.url_path_for("get_all_music"))

    musics.sort(key=lambda i: i.name.casefold())
    assert response.json() == {
        "items": [
            {
                "id": str(music.id),
                "name": music.name,
                "path": music.path,
            }
            for music in musics
        ],
        "pageInfo": {
            "number": 1,
            "amount": len(musics),
            "previousPageExists": False,
            "nextPageExists": False,
        },
    }
