from collections.abc import AsyncIterable

import httpx
import pytest
from fastapi import FastAPI

from app.adapters.api.app import create_fastapi


@pytest.fixture(scope="session")
def app() -> FastAPI:
    return create_fastapi()


@pytest.fixture(scope="session")
async def http_client(app: FastAPI) -> AsyncIterable[httpx.AsyncClient]:
    transport = httpx.ASGITransport(app=app)  # pyright: ignore[reportArgumentType]
    async with httpx.AsyncClient(
        transport=transport,
        base_url="http://test",
    ) as client:
        yield client
