import os

import pytest
from sqlalchemy.ext import asyncio

import app.db.base.engine


@pytest.fixture(scope="session")
def database_url() -> str:
    return os.environ["DATABASE_TEST_URL"]


@pytest.fixture(scope="session")
def async_sessionmaker() -> asyncio.async_sessionmaker[asyncio.AsyncSession]:
    return app.db.base.engine.async_session_factory
