from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import Depends, HTTPException
from fastapi.security import APIKeyHeader
from result import Err

from app.core.domain.user.commands import KeycloakAuthCommand
from app.core.domain.user.commands._keycloak_user import UserByKeycloakId
from app.db.models import User
from connectors.keycloak.schema import KeyCloakUserSchema

_scheme = APIKeyHeader(name="Authorization")

_unauthorization_error = HTTPException(
    status_code=HTTPStatus.UNAUTHORIZED,
)

class KeycloakAuth:

    @inject
    async def __call__(
        self,
        token: Annotated[str | None, Depends(_scheme)],
        command: Annotated[KeycloakAuthCommand, Inject],
    ) -> KeyCloakUserSchema:
        if token is None:
            raise _unauthorization_error
        token = token.removeprefix("Berear ")
        result = await command.execute(token)
        if isinstance(result, Err):
            raise _unauthorization_error
        return result.ok_value


class GetCurrentUser:
    @inject
    async def __call__(
        self,
        schema: Annotated[KeyCloakUserSchema, Depends(KeycloakAuth)],
        command: Annotated[UserByKeycloakId, Inject],
    ) -> User:
        return await command.execute(keycloak_id=schema.sub)


CurrentUser = Annotated[User, Depends(GetCurrentUser)]
Auth = Annotated[KeyCloakUserSchema, Depends(KeyCloakUserSchema)]
