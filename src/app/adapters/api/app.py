from collections.abc import AsyncIterator
from contextlib import aclosing, asynccontextmanager

from aioinject.ext.fastapi import AioInjectMiddleware
from fastapi import FastAPI
from sentry_sdk import init as sentry_init
from starlette.middleware.cors import CORSMiddleware

from app.core.di import get_container
from settings import FastApiSettings, get_settings

from .router import router


@asynccontextmanager
async def _lifespan(app: FastAPI) -> AsyncIterator[None]:
    async with (
        app.state.container,
        aclosing(app.state.container),
    ):
        yield


def create_fastapi() -> FastAPI:
    sentry_init()

    get_settings(FastApiSettings)

    app = FastAPI(lifespan=_lifespan)
    app.state.container = get_container()

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[],
        allow_origin_regex=None,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.add_middleware(AioInjectMiddleware, container=app.state.container)

    app.include_router(router)

    return app
