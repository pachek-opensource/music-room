from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject

from app.core.domain.music.commands import IncrementMusicPopularity
from lib.fastapi import PathId


class IncrementPopularity:
    @inject
    async def __call__(
        self,
        music_id: PathId,
        command: Annotated[IncrementMusicPopularity, Inject],
    ) -> None:
        await command.execute(music_id)
