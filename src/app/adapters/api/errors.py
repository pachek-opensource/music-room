from http import HTTPStatus
from typing import Self

from fastapi import HTTPException

from app.core.errors import EntityNotFoundError, RelationshipNotFoundError


class EntityNotFoundHTTPError(HTTPException):
    def __init__(
        self,
        id_: str,
        entity: str,
        headers: dict[str, str] | None = None,
    ) -> None:
        super().__init__(
            status_code=HTTPStatus.BAD_REQUEST,
            detail={
                "id": id_,
                "entity": entity,
            },
            headers=headers,
        )

    @classmethod
    def from_core(cls, error: EntityNotFoundError) -> Self:
        return cls(
            id_=error.id,
            entity=error.entity,
        )


class RelationshipNotFoundHTTPError(HTTPException):
    def __init__(
        self,
        id_: str,
        entity: str,
        headers: dict[str, str] | None = None,
    ) -> None:
        super().__init__(
            status_code=HTTPStatus.BAD_REQUEST,
            detail={
                "id": id_,
                "entity": entity,
            },
            headers=headers,
        )

    @classmethod
    def from_core(cls, error: RelationshipNotFoundError) -> Self:
        return cls(
            id_=error.id,
            entity=error.entity,
        )
