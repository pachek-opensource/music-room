import uuid
from collections.abc import Sequence
from typing import Self

from app.adapters.api.schemas.base import BaseSchema


class RoomSchema(BaseSchema):
    id: uuid.UUID
    name: str

    @classmethod
    def from_core_list(cls, models: Sequence[object]) -> Sequence[Self]:
        return [cls.model_validate(model) for model in models]


class RoomCreateSchema(BaseSchema):
    name: str
