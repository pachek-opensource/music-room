import uuid
from collections.abc import Sequence
from pathlib import PurePath
from typing import Annotated, Self

from pydantic import PlainSerializer

from app.adapters.api.schemas.base import BaseSchema


class MusicSchema(BaseSchema):
    id: uuid.UUID
    name: str
    path: Annotated[PurePath, PlainSerializer(PurePath.as_posix)]

    @classmethod
    def from_core_list(cls, models: Sequence[object]) -> Sequence[Self]:
        return [cls.model_validate(model) for model in models]


class GenreSchema(BaseSchema):
    id: uuid.UUID
    name: str

    @classmethod
    def from_core_list(cls, models: Sequence[object]) -> Sequence[Self]:
        return [cls.model_validate(model) for model in models]
