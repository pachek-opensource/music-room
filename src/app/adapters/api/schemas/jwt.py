from app.adapters.api.schemas.base import BaseSchema


class AuthTokensSchema(BaseSchema):
    access: str
    refresh: str


class LoginSchema(BaseSchema):
    username: str
    password: str
