import dataclasses
from collections.abc import Sequence
from typing import Annotated, Generic, TypeVar

from fastapi import Query
from pydantic import BaseModel, ConfigDict
from pydantic.alias_generators import to_camel


class BaseSchema(BaseModel):
    model_config = ConfigDict(
        alias_generator=to_camel,
        populate_by_name=True,
        from_attributes=True,
    )


TSchema = TypeVar("TSchema", bound=BaseSchema)


@dataclasses.dataclass
class PageParams:
    page_number: Annotated[int, Query()] = 1
    page_size: Annotated[int, Query()] = 30


class PageInfoSchema(BaseSchema):
    number: int
    amount: int
    previous_page_exists: bool
    next_page_exists: bool


class PaginationResultSchema(BaseSchema, Generic[TSchema]):
    items: Sequence[TSchema]
    page_info: PageInfoSchema
