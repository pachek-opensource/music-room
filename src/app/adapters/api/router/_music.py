import mimetypes
import uuid
from collections.abc import Sequence
from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter, Depends, Form, Header, UploadFile
from fastapi.responses import StreamingResponse
from result import Err

from app.adapters.api.auth import CurrentUser, KeycloakAuth
from app.adapters.api.dependencies import IncrementPopularity
from app.adapters.api.errors import (
    EntityNotFoundHTTPError,
    RelationshipNotFoundHTTPError,
)
from app.adapters.api.schemas.base import (
    PageInfoSchema,
    PageParams,
    PaginationResultSchema,
)
from app.adapters.api.schemas.music import GenreSchema, MusicSchema
from app.core.domain.music.commands import MusicCreateCommand
from app.core.domain.music.dto import MusicCreateDto
from app.core.domain.music.queries import (
    GenresQuery,
    MusicQuery,
    MusicsQuery,
    MusicStreamQuery,
)
from app.core.errors import RelationshipNotFoundError
from app.core.paginator import PageParamsDto
from lib.fastapi import PathId

router = APIRouter(
    prefix="/music",
    dependencies=[Depends(KeycloakAuth())],
)


@router.get("/")
@inject
async def get_all_music(
    page_params: Annotated[PageParams, Depends()],
    *,
    query: Annotated[MusicsQuery, Inject],
) -> PaginationResultSchema[MusicSchema]:
    page_params_dto = PageParamsDto.model_validate(page_params)
    result = await query.execute(page_params=page_params_dto)
    return PaginationResultSchema(
        items=MusicSchema.from_core_list(result.items),
        page_info=PageInfoSchema.model_validate(result.page_info),
    )


@router.get("/popularity")
@inject
async def get_popularity_music() -> Sequence[MusicSchema]:
    raise NotImplementedError


@router.post("/")
@inject
async def upload_music(
    file: UploadFile,
    name: Annotated[str, Form()],
    content_type: Annotated[str, Header()],
    genre_id: Annotated[uuid.UUID | None, Form()] = None,
    *,
    command: Annotated[MusicCreateCommand, Inject],
) -> MusicSchema:
    ext = mimetypes.guess_extension(content_type)

    filename = file.filename
    if filename is None:
        if ext is None:
            raise ValueError
        filename = name + ext

    result = await command.execute(
        MusicCreateDto(
            io=file.file,
            filename=filename,
            name=name,
            genre_ids=[genre_id] if genre_id is not None else [],
        ),
    )
    if isinstance(result, Err):
        match result.err_value:
            case RelationshipNotFoundError():
                raise RelationshipNotFoundHTTPError.from_core(result.err_value)

    return MusicSchema.model_validate(result.ok_value)


@router.get("/genres")
@inject
async def get_all_genres(
    page_params: Annotated[PageParams, Depends()],
    *,
    query: Annotated[GenresQuery, Inject],
) -> PaginationResultSchema[GenreSchema]:
    page_params_dto = PageParamsDto.model_validate(page_params)
    result = await query.execute(page_params=page_params_dto)
    return PaginationResultSchema(
        items=GenreSchema.from_core_list(result.items),
        page_info=PageInfoSchema.model_validate(result.page_info),
    )


@router.get("/history")
@inject
async def get_history(user: CurrentUser) -> None:
    pass


@router.get("/favorites")
@inject
async def get_favorites(user: CurrentUser) -> None:
    pass


@router.get("/{id}", dependencies=[Depends(IncrementPopularity())])
@inject
async def get_music(
    music_id: PathId,
    *,
    query: Annotated[MusicQuery, Inject],
) -> MusicSchema:
    result = await query.execute(music_id=music_id)
    return MusicSchema.model_validate(result)


@router.get("/{id}/stream", dependencies=[Depends(IncrementPopularity())])
@inject
async def get_music_stream(
    music_id: PathId,
    query: Annotated[MusicStreamQuery, Inject],
) -> StreamingResponse:
    result = await query.execute(music_id=music_id)
    if isinstance(result, Err):
        raise EntityNotFoundHTTPError.from_core(result.err_value)

    return StreamingResponse(result.ok_value)


@router.post("/{id}/favorite")
@inject
async def add_to_favorites(
    music_id: PathId,
    user: CurrentUser,
) -> None:
    pass


@router.delete("/{id}/favorite")
@inject
async def delete_from_favorites(
    music_id: PathId,
    user: CurrentUser,
) -> None:
    pass
