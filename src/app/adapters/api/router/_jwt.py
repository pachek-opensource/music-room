from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter, HTTPException, Path
from result import Err

from app.adapters.api.schemas.jwt import AuthTokensSchema, LoginSchema
from app.core.domain.user.commands._login import JwtLoginCommand
from app.core.domain.user.commands._refresh import JwtRefreshCommand

router = APIRouter(prefix="/jwt")


@router.post("/login")
@inject
async def jwt_login(
    body: LoginSchema,
    command: Annotated[JwtLoginCommand, Inject],
) -> AuthTokensSchema:
    # TODO(maksyutov.vlad): Можно удалить
    result = await command.execute(
        username=body.username,
        password=body.password,
    )
    if isinstance(result, Err):
        raise HTTPException(status_code=HTTPStatus.FORBIDDEN)
    return AuthTokensSchema.model_validate(result.ok_value)


@router.post("/refresh/{token}")
@inject
async def jwt_refresh(
    token: Annotated[str, Path()],
    command: Annotated[JwtRefreshCommand, Inject],
) -> AuthTokensSchema:
    # TODO(maksyutov.vlad): Можно удалить
    result = await command.execute(token=token)
    if isinstance(result, Err):
        raise HTTPException(status_code=HTTPStatus.FORBIDDEN)
    return AuthTokensSchema.model_validate(result.ok_value)
