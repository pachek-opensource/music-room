from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter, Body, Depends

from app.adapters.api.auth import KeycloakAuth
from app.adapters.api.schemas.base import (
    PageInfoSchema,
    PageParams,
    PaginationResultSchema,
)
from app.adapters.api.schemas.room import RoomCreateSchema, RoomSchema
from app.core.domain.room.commands import RoomCreateCommand
from app.core.domain.room.dto import RoomCreateDto
from app.core.domain.room.queries import RoomQuery, RoomsQuery
from app.core.paginator import PageParamsDto
from lib.fastapi import PathId

router = APIRouter(
    prefix="/room",
    dependencies=[Depends(KeycloakAuth())],
)


@router.get("/")
@inject
async def get_all_rooms(
    page_params: Annotated[PageParams, Depends()],
    *,
    query: Annotated[RoomsQuery, Inject],
) -> PaginationResultSchema[RoomSchema]:
    page_params_dto = PageParamsDto.model_validate(page_params)
    result = await query.execute(page_params=page_params_dto)
    return PaginationResultSchema(
        items=RoomSchema.from_core_list(result.items),
        page_info=PageInfoSchema.model_validate(result.page_info),
    )


@router.post("/")
@inject
async def create_room(
    body: Annotated[RoomCreateSchema, Body()],
    *,
    command: Annotated[RoomCreateCommand, Inject],
) -> RoomSchema:
    result = await command.execute(
        RoomCreateDto(
            name=body.name,
        ),
    )
    return RoomSchema.model_validate(result.ok_value)


@router.get("/{id}")
@inject
async def get_room(
    room_id: PathId,
    *,
    query: Annotated[RoomQuery, Inject],
) -> RoomSchema:
    result = await query.execute(room_id=room_id)
    return RoomSchema.model_validate(result)


@router.put("/{id}")
@inject
async def change_room(room_id: PathId) -> None:
    pass


@router.delete("/{id}")
@inject
async def delete_room(room_id: PathId) -> None:
    pass


@router.get("/{id}/musics")
@inject
async def get_musics(room_id: PathId) -> None:
    pass


@router.put("/{id}/musics")
@inject
async def add_musics(room_id: PathId) -> None:
    pass


@router.delete("/{id}/musics")
@inject
async def delete_musics(room_id: PathId) -> None:
    pass
