from fastapi import APIRouter

from ._jwt import router as jwt_router
from ._music import router as music_router
from ._room import router as room_router

router = APIRouter()

router.include_router(music_router)
router.include_router(room_router)
router.include_router(jwt_router)
