from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import DeclarativeBase


class UnitOfWork:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    def add(self, instance: DeclarativeBase) -> None:
        self._session.add(instance)

    async def save_changes(self) -> None:
        await self._session.flush()
