from typing import Self


class EntityNotFoundError(Exception):
    def __init__(self, entity: str, id_: str) -> None:
        self.entity = entity
        self.id = id_


class RelationshipNotFoundError(Exception):
    def __init__(self, entity: str, id_: str) -> None:
        self.entity = entity
        self.id = id_

    @classmethod
    def from_entity_not_found(cls, error: EntityNotFoundError) -> Self:
        return cls(
            entity=error.entity,
            id_=error.id,
        )


class AuthenticationFailedError(Exception):
    pass
