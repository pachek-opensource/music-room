import functools
import itertools
from collections.abc import Iterable, Sequence
from typing import Any

import aioinject
import aioinject.extensions
from aioinject.extensions import Extension
from pydantic_settings import BaseSettings

from settings import (
    DatabaseSettings,
    FastApiSettings,
    RedisSettings,
    S3Settings,
    SentrySettings,
    get_settings,
)

from .modules import (
    core,
    crypt,
    db,
    keycloak,
    music,
    redis,
    s3,
    user,
)

EXTENSIONS: Sequence[Extension] = [
    redis.InitTopKMusicPopularityExtension(),
]

PROVIDERS: Iterable[Iterable[aioinject.Provider[Any]]] = [
    db.providers,
    s3.providers,
    redis.providers,
    music.providers,
    core.providers,
    crypt.providers,
    user.providers,
    keycloak.providers,
]
SETTINGS = (
    DatabaseSettings,
    S3Settings,
    RedisSettings,
    FastApiSettings,
    SentrySettings,
)


def _register_settings(
    container: aioinject.Container,
    settings_classes: Iterable[type[BaseSettings]],
) -> None:
    for settings_cls in settings_classes:
        factory = functools.partial(get_settings, settings_cls)
        container.register(aioinject.Singleton(factory, type_=settings_cls))


@functools.cache
def get_container() -> aioinject.Container:
    container = aioinject.Container(EXTENSIONS)

    _register_settings(container, settings_classes=SETTINGS)

    for provider in itertools.chain.from_iterable(PROVIDERS):
        container.register(provider)

    return container
