from collections.abc import AsyncIterator, Iterable
from contextlib import asynccontextmanager
from typing import Any

import aioinject
from aioinject.extensions import LifespanExtension

from connectors.redis import (
    RedisClient,
    get_redis_client,
)
from settings import MusicPopularitySettings, get_settings


class InitTopKMusicPopularityExtension(LifespanExtension):
    @asynccontextmanager
    async def lifespan(
        self,
        container: aioinject.Container,
    ) -> AsyncIterator[None]:
        async with container.context() as ctx:
            redis = await ctx.resolve(RedisClient)
            settings = get_settings(MusicPopularitySettings)
            try:
                await redis.topk().info(settings.key)
            except:  # noqa: E722
                await redis.topk().reserve(settings.key, settings.count, 50, 4, 0.9)
            yield


providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Singleton(get_redis_client),
]
