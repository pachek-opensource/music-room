from collections.abc import Iterable
from typing import Any

import aioinject
from passlib.context import CryptContext


def make_crypt_context() -> CryptContext:
    return CryptContext()


providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Singleton(make_crypt_context),
]
