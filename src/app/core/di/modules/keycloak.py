from collections.abc import Iterable
from typing import Any

import aioinject

from connectors.keycloak.schema import KeyCloakUserSchema
from connectors.keycloak.service import KeycloakService
from settings import KeycloakSettings, get_settings


def _setup_keycloak() -> KeycloakService[KeyCloakUserSchema]:
    settings = get_settings(KeycloakSettings)
    return KeycloakService[KeyCloakUserSchema](
        server_url=settings.server_url,
        client_id=settings.client_id,
        realm_name=settings.realm_name,
        client_secret_key=settings.client_secret_key,
        encoding_algorithm=settings.encoding_algorithm,
    )


providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Singleton(_setup_keycloak, type_=KeycloakService[KeyCloakUserSchema]),
]
