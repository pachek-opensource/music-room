from collections.abc import Iterable
from typing import Any

import aioinject

from app.core.paginator import Paginator

providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Scoped(Paginator),
]
