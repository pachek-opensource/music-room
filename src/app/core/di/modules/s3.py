from __future__ import annotations

from collections.abc import Iterable
from typing import Any

import aioinject
from types_aiobotocore_s3 import S3Client

from app.core.storage.s3 import S3Storage
from connectors import s3
from settings import S3Settings


def get_s3_storage(client: S3Client, settings: S3Settings) -> S3Storage:
    return S3Storage(
        client=client,
        bucket=settings.bucket,
        endpoint_url=settings.endpoint_url,
        expires_in=settings.expires_in,
    )


providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Singleton(s3.get_s3_client, type_=S3Client),
    aioinject.Scoped(get_s3_storage),
]
