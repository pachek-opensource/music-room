from collections.abc import Iterable
from typing import Any

import aioinject

from app.core.domain.music.commands import IncrementMusicPopularity, MusicCreateCommand
from app.core.domain.music.mappers import MusicMapper
from app.core.domain.music.queries import (
    GenresQuery,
    MusicQuery,
    MusicsQuery,
    MusicStreamQuery,
)
from app.core.domain.music.repositories import MusicRepository
from app.core.domain.music.service import MusicService

providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Scoped(MusicRepository),
    aioinject.Scoped(MusicMapper),
    aioinject.Scoped(MusicService),
    aioinject.Scoped(MusicCreateCommand),
    aioinject.Scoped(IncrementMusicPopularity),
    aioinject.Scoped(MusicsQuery),
    aioinject.Scoped(MusicStreamQuery),
    aioinject.Scoped(MusicQuery),
    aioinject.Scoped(GenresQuery),
]
