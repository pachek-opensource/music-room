from collections.abc import Iterable
from typing import Any

import aioinject

from app.core.domain.user.commands import (
    JwtLoginCommand,
    JwtRefreshCommand,
    KeycloakAuthCommand,
    KeycloakSyncUser,
    UserByKeycloakId,
)
from app.core.domain.user.repositories import UserRepository

providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Scoped(UserRepository),
    aioinject.Scoped(KeycloakAuthCommand),
    aioinject.Scoped(JwtLoginCommand),
    aioinject.Scoped(JwtRefreshCommand),
    aioinject.Scoped(UserByKeycloakId),
    aioinject.Scoped(KeycloakSyncUser),
]
