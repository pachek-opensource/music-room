from collections.abc import Iterable
from typing import Any

import aioinject

from app.core.domain.room.commands import RoomCreateCommand
from app.core.domain.room.queries import RoomQuery, RoomsQuery
from app.core.domain.room.repositories import RoomRepository

providers: Iterable[aioinject.Provider[Any]] = [
    aioinject.Scoped(RoomRepository),
    aioinject.Scoped(RoomsQuery),
    aioinject.Scoped(RoomQuery),
    aioinject.Scoped(RoomCreateCommand),
]
