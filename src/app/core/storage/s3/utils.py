import uuid
from os import PathLike
from pathlib import PurePath


def build_random_filename(filepath: PathLike[str] | str) -> PurePath:
    if not isinstance(filepath, PurePath):
        filepath = PurePath(filepath)

    return filepath.with_stem(str(uuid.uuid4()))
