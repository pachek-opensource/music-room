import dataclasses
from collections.abc import Sequence
from typing import Generic, TypeVar

from sqlalchemy import Select, func, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.dto import BaseDto

TModel = TypeVar("TModel")


class PageParamsDto(BaseDto):
    page_number: int = 1
    page_size: int = 30


class PageInfoDto(BaseDto):
    number: int
    amount: int
    previous_page_exists: bool
    next_page_exists: bool


@dataclasses.dataclass
class PaginationResultDto(Generic[TModel]):
    items: Sequence[TModel]
    page_info: PageInfoDto


class Paginator:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def paginate(
        self,
        statement: Select[tuple[TModel]],
        page_params: PageParamsDto,
    ) -> PaginationResultDto[TModel]:
        stmt = statement.offset(
            (page_params.page_number - 1) * page_params.page_size,
        ).limit(page_params.page_size)
        amount_stmt = select(func.count()).select_from(statement.subquery())
        amount = await self._session.scalar(amount_stmt) or 0

        items: Sequence[TModel] = []
        if amount > 0:
            items = (await self._session.scalars(stmt)).all()

        return PaginationResultDto(
            items=items,
            page_info=PageInfoDto(
                number=page_params.page_number,
                amount=amount,
                previous_page_exists=page_params.page_number > 1,
                next_page_exists=(
                    amount > page_params.page_number * page_params.page_size
                ),
            ),
        )
