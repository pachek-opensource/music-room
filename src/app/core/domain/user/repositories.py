from collections.abc import Sequence

from sqlalchemy import Select, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import User

from .filters import UserFilter


class UserRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get_user(
        self,
        filter_: UserFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> User | None:
        stmt = self.get_user_stmt(filter_=filter_, options=options).limit(2)
        return (await self._session.scalars(stmt)).one_or_none()

    async def get_users(
        self,
        filter_: UserFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Sequence[User]:
        stmt = self.get_user_stmt(filter_=filter_, options=options)
        return (await self._session.scalars(stmt)).all()

    def get_user_stmt(
        self,
        filter_: UserFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Select[tuple[User]]:
        stmt = select(User)
        if filter_ is not None:
            stmt = filter_.apply(stmt)
        if options is not None:
            stmt = stmt.options(*options)
        return stmt
