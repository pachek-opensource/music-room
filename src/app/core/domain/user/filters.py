import uuid
from collections.abc import Sequence
from typing import Annotated

from sqla_filter import UNSET, BaseFilter, FilterField, Unset
from sqlalchemy.sql.operators import eq, in_op

from app.db.models import User


class UserFilter(BaseFilter):
    ids: Annotated[
        Sequence[uuid.UUID] | Unset,
        FilterField(User.id, operator=in_op),
    ] = UNSET
    username: Annotated[
        str | Unset,
        FilterField(User.username, operator=eq),
    ] = UNSET
    keycloak_id: Annotated[
        uuid.UUID | Unset,
        FilterField(User.keycloak_id, operator=eq),
    ] = UNSET
