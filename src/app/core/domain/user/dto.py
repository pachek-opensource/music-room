import dataclasses
import uuid
from typing import Literal

from app.core.dto import BaseDto


class _TokenBase(BaseDto):
    exp: int
    jti: str
    user_id: uuid.UUID
    username: str


class AccessTokenDTO(_TokenBase):
    token_type: Literal["access"]


class RefreshTokenDTO(_TokenBase):
    token_type: Literal["refresh"]


@dataclasses.dataclass(slots=True, frozen=True)
class AuthTokensDTO:
    access: str
    refresh: str
