from datetime import datetime

from result import Err, Ok, Result

from app.core.domain.user.dto import AccessTokenDTO, AuthTokensDTO
from app.core.domain.user.filters import UserFilter
from app.core.domain.user.repositories import UserRepository
from app.core.errors import AuthenticationFailedError
from app.db.models import User
from lib.dates import utc_now
from lib.pyjwt import JwtService
from settings import AuthSettings


class AuthService:
    def __init__(
        self,
        repository: UserRepository,
        jwt_service: JwtService,
        settings: AuthSettings,
    ) -> None:
        self._repository = repository
        self._jwt_service = jwt_service
        self._settings = settings

    async def authenticate(
        self,
        username: str,
        password: str,
    ) -> Result[User, AuthenticationFailedError]:
        user = await self._repository.get_user(UserFilter(username=username))
        if user is None or user.password is None:
            return Err(AuthenticationFailedError())

        if not self._jwt_service.verify(
            password=password,
            hashed_password=user.password,
        ):
            return Err(AuthenticationFailedError())

        return Ok(user)

    def tokens(self, user: User) -> AuthTokensDTO:
        now = utc_now()
        access_dto = self._access_token_dto(user=user, now=now)

        payload = access_dto.model_dump()
        return AuthTokensDTO(
            access=self._jwt_service.encode(payload=payload),
            refresh=self._jwt_service.encode(
                payload=payload
                | {
                    "token_type": "refresh",
                    "exp": int((now + self._settings.jwt_refresh_lifetime).timestamp()),
                },
            ),
        )

    def _access_token_dto(
        self,
        user: User,
        now: datetime,
    ) -> AccessTokenDTO:
        return AccessTokenDTO(
            token_type="access",  # noqa: S106
            exp=int((now + self._settings.jwt_access_lifetime).timestamp()),
            jti="",
            user_id=user.id,
            username=user.username,
        )
