from uuid import UUID

from app.core.domain.user.filters import UserFilter
from app.core.domain.user.repositories import UserRepository
from app.db.models._user import User


class UserByKeycloakId:
    def __init__(self, repository: UserRepository) -> None:
        self._repository = repository

    async def execute(self, keycloak_id: UUID) -> User:
        user = await self._repository.get_user(
            filter_=UserFilter(keycloak_id=keycloak_id),
        )
        if user is None:
            raise ValueError
        return user
