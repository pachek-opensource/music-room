

import jwt
from result import Result

from connectors.keycloak.schema import KeyCloakUserSchema
from connectors.keycloak.service import KeycloakService


class KeycloakAuthCommand:
    def __init__(self, service: KeycloakService[KeyCloakUserSchema]) -> None:
        self._service = service

    async def execute(self, token: str) -> Result[
        KeyCloakUserSchema,
        jwt.ExpiredSignatureError | jwt.DecodeError | jwt.InvalidAudienceError,
    ]:
        return await self._service.decode_token(token=token)
