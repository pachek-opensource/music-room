from result import Err, Ok, Result

from app.core.domain.user.dto import AuthTokensDTO
from app.core.domain.user.service import AuthService
from app.core.errors import AuthenticationFailedError


class JwtLoginCommand:
    def __init__(
        self,
        auth_service: AuthService,
    ) -> None:
        self._service = auth_service

    async def execute(
        self,
        username: str,
        password: str,
    ) -> Result[AuthTokensDTO, AuthenticationFailedError]:
        user = await self._service.authenticate(username=username, password=password)
        if isinstance(user, Err):
            return user
        return Ok(self._service.tokens(user.ok_value))
