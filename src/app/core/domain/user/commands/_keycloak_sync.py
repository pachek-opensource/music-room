from collections.abc import Mapping, Sequence
from typing import Any
from uuid import UUID

from keycloak.keycloak_admin import KeycloakAdmin
from pydantic import BaseModel, TypeAdapter

from app.core.domain.user.repositories import UserRepository
from settings import KeycloakAdminSettings, get_settings


class _UserSchema(BaseModel):
    id: UUID
    username: str
    enabled: bool
    attributes: Mapping[str, Sequence[str]]


_adapter = TypeAdapter(list[_UserSchema])


class KeycloakSyncUser:
    def __init__(
        self,
        repository: UserRepository,
    ) -> None:
        settings = get_settings(KeycloakAdminSettings)
        self._service = KeycloakAdmin(
            server_url=settings.server_url,
            realm_name=settings.realm_name,
            username=settings.username,
            password=settings.password,
        )
        self._repository = repository

    async def execute(self) -> None:
        await self._service.connect()

        users: Any = (await self._service.get_users())
        schemas = _adapter.validate_python(users)
        print(schemas)  # noqa: T201
        # TODO(maksyutov.vlad): Дописать создание/обновление пользователей
