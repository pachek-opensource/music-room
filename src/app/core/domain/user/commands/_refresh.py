from pydantic import ValidationError
from result import Err, Ok, Result

from app.core.domain.user.dto import AuthTokensDTO, RefreshTokenDTO
from app.core.domain.user.filters import UserFilter
from app.core.domain.user.repositories import UserRepository
from app.core.domain.user.service import AuthService
from app.core.errors import AuthenticationFailedError
from lib.pyjwt import JwtService


class JwtRefreshCommand:
    def __init__(
        self,
        jwt_service: JwtService,
        auth_service: AuthService,
        user_repository: UserRepository,
    ) -> None:
        self._jwt_service = jwt_service
        self._auth_service = auth_service
        self._repository = user_repository

    async def execute(
        self,
        token: str,
    ) -> Result[AuthTokensDTO, AuthenticationFailedError]:
        try:
            user_info = RefreshTokenDTO.model_validate(self._jwt_service.decode(token))
        except ValidationError:
            return Err(AuthenticationFailedError())

        user = await self._repository.get_user(UserFilter(ids=[user_info.user_id]))
        if user is None:
            return Err(AuthenticationFailedError())

        return Ok(self._auth_service.tokens(user))
