from collections.abc import Sequence

from sqlalchemy import Select, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import Room

from .filters import RoomFilter


class RoomRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get_room(
        self,
        filter_: RoomFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Room | None:
        stmt = self.get_room_stmt(filter_=filter_, options=options)
        return (await self._session.scalars(stmt)).one_or_none()

    async def get_rooms(
        self,
        filter_: RoomFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Sequence[Room] | None:
        stmt = self.get_room_stmt(filter_=filter_, options=options)
        return (await self._session.scalars(stmt)).all()

    @classmethod
    def get_room_stmt(
        cls,
        filter_: RoomFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Select[tuple[Room]]:
        stmt = select(Room)
        if options is not None:
            stmt = stmt.options(*options)
        if filter_ is not None:
            stmt = filter_.apply(stmt)
        return stmt
