import uuid

from app.core.paginator import PageParamsDto, PaginationResultDto, Paginator
from app.db.models import Room

from .filters import RoomFilter
from .repositories import RoomRepository


class RoomsQuery:
    def __init__(
        self,
        repository: RoomRepository,
        paginator: Paginator,
    ) -> None:
        self._repository = repository
        self._paginator = paginator

    async def execute(
        self,
        *,
        filter_: RoomFilter | None = None,
        page_params: PageParamsDto,
    ) -> PaginationResultDto[Room]:
        stmt = self._repository.get_room_stmt(filter_=filter_)
        return await self._paginator.paginate(stmt, page_params=page_params)


class RoomQuery:
    def __init__(
        self,
        repository: RoomRepository,
    ) -> None:
        self._repository = repository

    async def execute(
        self,
        *,
        room_id: uuid.UUID,
    ) -> Room | None:
        return await self._repository.get_room(filter_=RoomFilter(ids=[room_id]))
