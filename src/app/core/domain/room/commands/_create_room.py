from result import Ok

from app.core.domain.room.dto import RoomCreateDto, RoomDto
from app.core.uow import UnitOfWork
from app.db.models import Room


class RoomCreateCommand:
    def __init__(
        self,
        uow: UnitOfWork,
    ) -> None:
        self._uow = uow

    async def execute(
        self,
        dto: RoomCreateDto,
    ) -> Ok[RoomDto]:
        room = Room(name=dto.name)
        self._uow.add(room)
        await self._uow.save_changes()
        return Ok(RoomDto(id=room.id, name=room.name))
