import uuid
from dataclasses import dataclass


@dataclass(frozen=True, slots=True, kw_only=True)
class RoomCreateDto:
    name: str


@dataclass(frozen=True, slots=True, kw_only=True)
class RoomDto:
    id: uuid.UUID
    name: str
