import uuid

from app.db.models import Music, Room
from connectors.redis import RedisClient

from .constant import RoomType
from .dto import QueueDto


class QueueService:
    def __init__(self, redis: RedisClient) -> None:
        self._redis = redis

    async def add_music(
        self,
        *,
        room: Room,
        music: Music,
        room_type: RoomType,
    ) -> None:
        """Добавляет в конец очереди id музыки"""
        key = self._build_key(room, room_type)
        await self._redis.lpush(key, str(music.id))

    async def pop_music(self, room: Room) -> Music:
        """
        Достает музыку сначала из премиум очереди,
        если она пуста, то из бесплатной
        """
        for room_type in (RoomType.premium, RoomType.free):
            key = self._build_key(room, room_type)
            id_: str | None = (  # pyright: ignore[reportUnknownVariableType]
                await self._redis.rpop(key)
            )
            if id_ is not None:
                return Music(
                    id=uuid.UUID(id_),  # pyright: ignore[reportUnknownArgumentType]
                )

        raise NotImplementedError

    async def get_queue(self, room: Room) -> QueueDto:
        """Пока не знаю как лучше, либо возвращать Sequence[Music], либо QueueDto"""
        result = QueueDto()
        for enum, array in [
            (RoomType.premium, result.premium),
            (RoomType.free, result.free),
        ]:
            key = self._build_key(room, enum)
            queue = await self._redis.lrange(key, 0, -1)
            # TODO(maksyutov.vlad): Заглушка
            array.extend(Music(id=item) for item in queue)

        return result

    def _build_key(
        self,
        room: Room,
        room_type: RoomType,
    ) -> str:
        return f"room:{room.id}:{room_type}"
