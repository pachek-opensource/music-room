import enum


class RoomType(enum.StrEnum):
    premium = enum.auto()
    free = enum.auto()
