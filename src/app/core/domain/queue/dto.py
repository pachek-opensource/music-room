import dataclasses

from app.db.models import Music


@dataclasses.dataclass(slots=True, kw_only=True)
class QueueDto:
    premium: list[Music] = dataclasses.field(default_factory=list)
    free: list[Music] = dataclasses.field(default_factory=list)
