import uuid
from collections.abc import Sequence
from typing import Annotated

from sqla_filter import UNSET, BaseFilter, FilterField, Unset
from sqlalchemy.sql.operators import in_op

from app.db.models import Genre, Music


class MusicFilter(BaseFilter):
    ids: Annotated[
        Sequence[uuid.UUID] | Unset,
        FilterField(Music.id, operator=in_op),
    ] = UNSET

    genre_ids: Annotated[
        Sequence[uuid.UUID] | Unset,
        FilterField(Music.genres, operator=in_op),
    ] = UNSET


class GenreFilter(BaseFilter):
    ids: Annotated[
        Sequence[uuid.UUID] | Unset,
        FilterField(Genre.id, operator=in_op),
    ] = UNSET
