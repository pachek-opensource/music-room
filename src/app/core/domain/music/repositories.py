from collections.abc import Sequence

from sqlalchemy import Select, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import Genre, Music

from .filters import GenreFilter, MusicFilter


class MusicRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get_gernes(
        self,
        filter_: GenreFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Sequence[Genre]:
        stmt = self.get_genre_stmt(filter_=filter_, options=options)
        return (await self._session.scalars(stmt)).all()

    async def get_musics(
        self,
        filter_: MusicFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Sequence[Music]:
        stmt = self.get_music_stmt(filter_=filter_, options=options)
        return (await self._session.scalars(stmt)).all()

    async def get_music(
        self,
        filter_: MusicFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Music | None:
        stmt = self.get_music_stmt(filter_=filter_, options=options)
        return (await self._session.scalars(stmt)).one_or_none()

    @classmethod
    def get_music_stmt(
        cls,
        filter_: MusicFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Select[tuple[Music]]:
        stmt = select(Music).order_by(Music.name)

        if filter_ is not None:
            stmt = filter_.apply(stmt)
        if options is not None:
            stmt = stmt.options(*options)

        return stmt

    @classmethod
    def get_genre_stmt(
        cls,
        filter_: GenreFilter | None = None,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Select[tuple[Genre]]:
        stmt = select(Genre).order_by(Genre.name)
        if filter_ is not None:
            stmt = filter_.apply(stmt)
        if options is not None:
            stmt = stmt.options(*options)

        return stmt
