import uuid
from collections.abc import AsyncIterator

from result import Err, Ok, Result

from app.core.domain.music.filters import GenreFilter, MusicFilter
from app.core.errors import EntityNotFoundError
from app.core.paginator import PageParamsDto, PaginationResultDto, Paginator
from app.core.storage.s3.storage import S3Storage
from app.db.models import Genre, Music

from .repositories import MusicRepository


class MusicsQuery:
    def __init__(
        self,
        repository: MusicRepository,
        paginator: Paginator,
    ) -> None:
        self._repository = repository
        self._paginator = paginator

    async def execute(
        self,
        *,
        filter_: MusicFilter | None = None,
        page_params: PageParamsDto,
    ) -> PaginationResultDto[Music]:
        stmt = self._repository.get_music_stmt(filter_=filter_)
        return await self._paginator.paginate(stmt, page_params=page_params)


class MusicStreamQuery:
    def __init__(
        self,
        repository: MusicRepository,
        storage: S3Storage,
    ) -> None:
        self._repository = repository
        self._storage = storage

    async def execute(self, music_id: uuid.UUID) -> Result[
        AsyncIterator[bytes],
        EntityNotFoundError,
    ]:
        music = await self._repository.get_music(filter_=MusicFilter(ids=[music_id]))
        if music is None:
            return Err(
                EntityNotFoundError(
                    entity=Music.__qualname__,
                    id_=str(music_id),
                ),
            )
        return Ok(self._storage.stream_object(music.path))


class GenresQuery:
    def __init__(
        self,
        repository: MusicRepository,
        paginator: Paginator,
    ) -> None:
        self._repository = repository
        self._paginator = paginator

    async def execute(
        self,
        *,
        filter_: GenreFilter | None = None,
        page_params: PageParamsDto,
    ) -> PaginationResultDto[Genre]:
        stmt = self._repository.get_genre_stmt(filter_=filter_)
        return await self._paginator.paginate(stmt, page_params=page_params)


class MusicQuery:
    def __init__(
        self,
        repository: MusicRepository,
    ) -> None:
        self._repository = repository

    async def execute(
        self,
        *,
        music_id: uuid.UUID,
    ) -> Music | None:
        return await self._repository.get_music(filter_=MusicFilter(ids=[music_id]))
