import uuid
from collections.abc import Sequence

from app.core.domain.music.filters import MusicFilter
from app.core.domain.music.repositories import MusicRepository
from app.db.models import Music
from connectors.redis import RedisClient
from settings import MusicPopularitySettings, get_settings


class MusicService:
    def __init__(
        self,
        redis: RedisClient,
        repository: MusicRepository,
    ) -> None:
        self._redis = redis
        self._repository = repository
        self._settings = get_settings(MusicPopularitySettings)

    async def top_popularities(self) -> Sequence[Music]:
        keys: Sequence[  # pyright: ignore[reportUnknownVariableType]
            uuid.UUID
        ] = await self._redis.topk().list(
            self._settings.key,
        )
        musics = {
            music.id: music
            for music in await self._repository.get_musics(
                MusicFilter(ids=keys),  # pyright: ignore[reportUnknownArgumentType]
            )
        }
        return [
            musics[key] for key in keys  # pyright: ignore[reportUnknownVariableType]
        ]

    async def increment_music(self, music_id: uuid.UUID) -> None:
        await self._redis.topk().add(self._settings.key, str(music_id))
