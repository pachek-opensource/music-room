import uuid
from collections.abc import Sequence
from dataclasses import dataclass
from pathlib import PurePath
from typing import BinaryIO


@dataclass(slots=True, frozen=True, kw_only=True)
class MusicCreateDto:
    io: BinaryIO
    filename: str

    name: str
    genre_ids: Sequence[uuid.UUID]


@dataclass(slots=True, frozen=True, kw_only=True)
class MusicDto:
    id: uuid.UUID
    name: str
    path: PurePath
    url: str
