import uuid
from collections.abc import Sequence
from pathlib import PurePath

import slugify
from result import Err, Ok, Result

from app.core.domain.music.dto import MusicCreateDto, MusicDto
from app.core.domain.music.filters import GenreFilter
from app.core.domain.music.mappers import MusicMapper
from app.core.domain.music.repositories import MusicRepository
from app.core.errors import RelationshipNotFoundError
from app.core.storage.s3 import S3Storage
from app.core.storage.s3.dto import UploadFileDTO
from app.core.uow import UnitOfWork
from app.db.models import Genre, Music


class MusicCreateCommand:
    def __init__(
        self,
        storage: S3Storage,
        repository: MusicRepository,
        uow: UnitOfWork,
        mapper: MusicMapper,
    ) -> None:
        self._storage = storage
        self._repository = repository
        self._mapper = mapper
        self._uow = uow

    async def execute(
        self,
        dto: MusicCreateDto,
    ) -> Result[MusicDto, RelationshipNotFoundError]:
        genres = await self._get_genres(dto.genre_ids)
        if isinstance(genres, Err):
            return genres

        filename = slugify.slugify(dto.filename)
        upload_dto = UploadFileDTO(
            io=dto.io,
            path=PurePath("music", filename),
        )
        async with self._storage.upload_object(upload_dto) as uploaded_file:
            music = Music(
                name=dto.name,
                path=uploaded_file.path,
                genres=genres.ok_value,  # TODO(maksyutov.vlad): Доделать
            )
            self._uow.add(music)
            await self._uow.save_changes()

        return Ok(await self._mapper.map(music, uploaded_dto=uploaded_file))

    async def _get_genres(
        self,
        genre_ids: Sequence[uuid.UUID],
    ) -> Result[Sequence[Genre], RelationshipNotFoundError]:
        genres = await self._repository.get_gernes(GenreFilter(ids=genre_ids))
        if len(genre_ids) == len(genres):
            return Ok(genres)

        unfound_genre_ids = set(genre_ids).difference(genre.id for genre in genres)
        return Err(
            RelationshipNotFoundError(
                entity=Genre.__qualname__,
                id_=", ".join(map(str, unfound_genre_ids)),
            ),
        )
