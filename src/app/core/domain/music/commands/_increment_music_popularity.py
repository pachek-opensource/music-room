import uuid

from app.core.domain.music.service import MusicService


class IncrementMusicPopularity:
    def __init__(
        self,
        service: MusicService,
    ) -> None:
        self._service = service

    async def execute(self, music_id: uuid.UUID) -> None:
        await self._service.increment_music(music_id=music_id)
