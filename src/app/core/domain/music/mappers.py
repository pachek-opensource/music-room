from app.core.domain.music.dto import MusicDto
from app.core.storage.s3.dto import UploadedFileDTO
from app.core.storage.s3.storage import S3Storage
from app.db.models import Music


class MusicMapper:
    def __init__(
        self,
        storage: S3Storage,
    ) -> None:
        self._storage = storage

    async def map(
        self,
        music: Music,
        *,
        uploaded_dto: UploadedFileDTO | None = None,
    ) -> MusicDto:

        match uploaded_dto:
            case UploadedFileDTO():
                url = uploaded_dto.url
            case None:
                url = await self._storage.create_url(music.path)

        return MusicDto(
            id=music.id,
            name=music.name,
            path=music.path,
            url=url,
        )
