import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.base import Base, uuid_pk

from ._music import Genre


class Room(Base):
    __tablename__ = "room"

    id: Mapped[uuid_pk]
    name: Mapped[str]

    genres: Mapped[list[Genre]] = relationship(secondary="room_genre")


class RoomGenre(Base):
    __tablename__ = "room_genre"

    room_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("room.id"),
        primary_key=True,
    )
    genre_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("genre.id"),
        primary_key=True,
    )
