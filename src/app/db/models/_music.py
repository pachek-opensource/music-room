import uuid
from pathlib import PurePath

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.base import Base, uuid_pk


class Genre(Base):
    __tablename__ = "genre"

    id: Mapped[uuid_pk]
    name: Mapped[str]

    music: Mapped[list["Music"]] = relationship(
        secondary="music_genre",
        back_populates="genres",
    )


class Music(Base):
    __tablename__ = "music"

    id: Mapped[uuid_pk]
    name: Mapped[str]
    path: Mapped[PurePath]

    genres: Mapped[list[Genre]] = relationship(
        secondary="music_genre",
        back_populates="music",
    )


class MusicGenre(Base):
    __tablename__ = "music_genre"

    music_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("music.id"),
        primary_key=True,
    )
    genre_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("genre.id"),
        primary_key=True,
    )


class MusicFavorite(Base):
    __tablename__ = "music_favorite"

    music_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("music.id"),
        primary_key=True,
    )
    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user.id"),
        primary_key=True,
    )


class MusicHistory(Base):
    __tablename__ = "music_history"

    music_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("music.id"),
        primary_key=True,
    )
    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user.id"),
        primary_key=True,
    )
