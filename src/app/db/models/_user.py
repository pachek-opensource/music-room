from __future__ import annotations

import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.base import Base, uuid_pk
from app.db.constant import UserSocialEnum

from ._music import Music


class User(Base):
    __tablename__ = "user"

    id: Mapped[uuid_pk]
    username: Mapped[str] = mapped_column(unique=True)
    password: Mapped[str | None]

    keycloak_id: Mapped[uuid.UUID] = mapped_column(unique=True)

    socials: Mapped[list[UserSocial]] = relationship(back_populates="user")
    favorites: Mapped[list[Music]] = relationship(secondary="music_favorite")
    history: Mapped[list[Music]] = relationship(secondary="music_history")


class UserSocial(Base):
    __tablename__ = "user_social"

    user_id: Mapped[uuid_pk] = mapped_column(ForeignKey("user.id"), primary_key=True)
    social: Mapped[UserSocialEnum] = mapped_column(primary_key=True)
    social_id: Mapped[uuid.UUID] = mapped_column(unique=True)

    user: Mapped[User] = relationship(back_populates="socials")


class Payment(Base):
    __tablename__ = "payment"

    id: Mapped[uuid_pk]
    user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey("user.id"), index=True)
