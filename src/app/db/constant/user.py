import enum


class UserSocialEnum(enum.Enum):
    telegram = enum.auto()
    discord = enum.auto()
    vk = enum.auto()
