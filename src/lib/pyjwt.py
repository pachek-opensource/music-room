from collections.abc import Mapping

import jwt
from passlib.context import CryptContext
from result import Err, Ok, Result

from settings import AuthSettings


class AuthorizationError(Exception): ...


class JwtService:
    def __init__(self, settings: AuthSettings, crypt: CryptContext) -> None:
        self._settings = settings
        self._crypt_context = crypt

    def encode(self, payload: dict[str, object]) -> str:
        return jwt.encode(
            payload=payload,
            key=self._settings.private_key,
            algorithm=self._settings.algorithm,
        )

    def decode(self, token: str) -> Result[
        Mapping[str, object],
        AuthorizationError,
    ]:
        try:
            return Ok(
                jwt.decode(
                    jwt=token,
                    key=self._settings.public_key,
                    algorithms=[self._settings.algorithm],
                ),
            )
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return Err(AuthorizationError())

    def verify(
        self,
        password: str,
        hashed_password: str,
    ) -> bool:
        return self._crypt_context.verify(
            secret=password,
            hash=hashed_password,
        )
