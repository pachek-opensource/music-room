from abc import abstractmethod
from collections.abc import Sequence
from typing import Protocol, Self


class OrmMixin(Protocol):
    @classmethod
    @abstractmethod
    def from_core(cls, model: object) -> Self: ...

    @classmethod
    def from_core_list(cls, models: Sequence[object]) -> Sequence[Self]:
        return [cls.from_core(model) for model in models]
