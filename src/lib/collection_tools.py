from collections.abc import AsyncIterable, Iterable, Iterator, Sequence
from itertools import islice
from typing import TypeVar

T = TypeVar("T")


def batched(iterable: Iterable[T], n: int) -> Iterator[Sequence[T]]:
    iterator = iter(iterable)
    while batch := tuple(islice(iterator, n)):
        yield batch


async def async_batched(
    iterable: AsyncIterable[T],
    batch_size: int,
) -> AsyncIterable[Sequence[T]]:
    buffer: list[T] = []
    async for element in iterable:
        buffer.append(element)
        if len(buffer) >= batch_size:
            yield buffer
            buffer = []

    if buffer:
        yield buffer
