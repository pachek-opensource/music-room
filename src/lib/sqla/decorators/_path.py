from pathlib import PurePath

from sqlalchemy import VARCHAR, Dialect, TypeDecorator


class PurePathDecorator(TypeDecorator[PurePath]):
    cache_ok = False
    impl = VARCHAR

    def process_bind_param(
        self,
        value: PurePath | None,
        dialect: Dialect,  # noqa: ARG002
    ) -> str | None:
        if not value:
            return None
        return PurePath(value).as_posix()

    def process_result_value(
        self,
        value: str | None,
        dialect: Dialect,  # noqa: ARG002
    ) -> PurePath | None:
        if not value:
            return None
        return PurePath(value)
