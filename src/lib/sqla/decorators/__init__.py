from ._path import PurePathDecorator

__all__ = [
    "PurePathDecorator",
]
