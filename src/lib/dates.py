import calendar
from datetime import (
    UTC,
    date,
    datetime,
    timedelta,
    timezone,
)

MOSCOW_TZ = timezone(offset=timedelta(hours=3))


def moscow_now() -> datetime:
    return datetime.now(tz=MOSCOW_TZ)


def moscow_today() -> date:
    return datetime.now(tz=MOSCOW_TZ).date()


def utc_now() -> datetime:
    return datetime.now(tz=UTC)


def utc_today() -> date:
    return datetime.now(tz=UTC).date()


def month_start(date_: date) -> date:
    return date_.replace(day=1)


def month_end(date_: date) -> date:
    _, day = calendar.monthrange(year=date_.year, month=date_.month)
    return date(date_.year, date_.month, day)
