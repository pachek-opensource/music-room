from __future__ import annotations

import contextlib
from collections.abc import AsyncIterator
from typing import TYPE_CHECKING

from aioboto3 import Session

from settings import S3Settings

if TYPE_CHECKING:
    from types_aiobotocore_s3 import S3Client


@contextlib.asynccontextmanager
async def get_s3_client(settings: S3Settings) -> AsyncIterator[S3Client]:
    session = Session(
        aws_access_key_id=settings.access_key_id,
        aws_secret_access_key=settings.secret_access_key,
    )
    client: S3Client
    async with session.client(  # pyright: ignore[reportGeneralTypeIssues]
        service_name="s3",
        endpoint_url=settings.endpoint_url,
    ) as client:  # pyright: ignore[reportUnknownVariableType]
        yield client
