from typing import Annotated, Any, TypeVar
from uuid import UUID

from pydantic import BaseModel, BeforeValidator, ConfigDict, Field
from pydantic.alias_generators import to_camel

_T = TypeVar("_T")


def _get_first(value: list[Any]) -> Any:  # noqa: ANN401
    if not value:
        return None

    return value[0]


FirstItem = Annotated[_T, BeforeValidator(_get_first)]


class _BaseModel(BaseModel):
    model_config = ConfigDict(alias_generator=to_camel, populate_by_name=True)


class KeyCloakUserAttributesSchema(_BaseModel):
    employee_type: list[UUID] | None = None
    create_timestamp: list[str] | None = None
    modify_timestamp: list[str] | None = None
    ldap_entry_dn: list[str | dict[str, str]] | None = Field(
        None,
        alias="LDAP_ENTRY_DN",
    )
    ldap_id: list[str] | None = Field(None, alias="LDAP_ID")
    realm: list[str] | None = None

    full_name: FirstItem[str] | None = None
    middle_name: FirstItem[str] | None = Field(None, alias="serName")
    avatar_url: FirstItem[str] | None = Field(None, alias="avatar")

    phone: FirstItem[str] | None = None


class KeyCloakUserSchema(_BaseModel):
    exp: int
    iat: int
    auth_time: int
    jti: UUID
    iss: str
    sub: UUID
    typ: str
    azp: str
    nonce: UUID
    session_state: UUID
    acr: float
    allowed_origins: Annotated[list[str], Field(alias="allowed-origins")]
    scope: str
    sid: UUID
    email_verified: bool
    preferred_username: str
    attributes: KeyCloakUserAttributesSchema | None = None
