import contextlib
from collections.abc import AsyncIterator
from typing import TYPE_CHECKING, Any, cast

from redis.asyncio import Redis

from settings import RedisSettings

if TYPE_CHECKING:
    RedisClient = Redis[Any]
else:
    RedisClient = Redis


@contextlib.asynccontextmanager
async def get_redis_client(
    settings: RedisSettings,
) -> AsyncIterator[RedisClient]:
    async with Redis(
        host=settings.host,
        port=settings.port,
        db=settings.database,
        decode_responses=True,
    ) as client:
        yield cast(RedisClient, client)
