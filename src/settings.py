import functools
from datetime import timedelta
from typing import Literal, TypeVar
from urllib.parse import quote_plus

from pydantic_settings import BaseSettings, SettingsConfigDict

TSettings = TypeVar("TSettings", bound=BaseSettings)


def get_settings(cls: type[TSettings]) -> TSettings:
    return cls()


get_settings = functools.cache(get_settings)  # Mypy moment


class DatabaseSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="database_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    driver: str = "postgresql+asyncpg"
    name: str
    user: str
    password: str
    host: str

    echo: bool = False

    @property
    def url(self) -> str:
        password = quote_plus(self.password)
        return f"{self.driver}://{self.user}:{password}@{self.host}/{self.name}"


class RedisSettings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="redis_")

    host: str
    port: int = 6379
    database: int = 0


class FastApiSettings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="api_")


class S3Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="s3_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    endpoint_url: str
    bucket: str
    access_key_id: str
    secret_access_key: str
    expires_in: int = 3600


class AuthSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="auth_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    public_key: str
    private_key: str = ""
    algorithm: Literal["RS256"] = "RS256"

    jwt_access_lifetime: timedelta = timedelta(minutes=5)
    jwt_refresh_lifetime: timedelta = timedelta(days=10)


class KeycloakSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="keycloak_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    server_url: str
    client_id: str
    realm_name: str
    client_secret_key: str
    encoding_algorithm: Literal["RS256"] = "RS256"


class KeycloakAdminSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="keycloak_admin_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    server_url: str
    realm_name: str
    username: str = "admin"
    password: str = "admin"

class SentrySettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="sentry_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    dsn: str = ""
    environment: str = "production"
    traces_sample_rate: float = 1.0


class MusicPopularitySettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="popularity_",
        case_sensitive=False,
        str_strip_whitespace=True,
    )

    key: Literal["music-popularity"] = "music-popularity"
    count: int = 20
