FROM python:3.11-slim as requirements

RUN pip install pdm
COPY ./pyproject.toml ./pdm.lock ./
RUN pdm export --prod -f requirements -o requirements.txt


FROM python:3.11-slim as build

COPY --from=requirements requirements.txt .

RUN pip install uv
RUN uv venv

RUN uv pip install --no-cache-dir -r requirements.txt


FROM python:3.11-slim
ENV PYTHONPATH=$PYTHONPATH:/app/src \
    PATH=$PATH:/home/app/.local/bin \
    PYTHONUNBUFFERED=1

RUN apt-get update && apt-get install -y git

WORKDIR /app

COPY --from=build .venv .venv

RUN . .venv/bin/activate

COPY ./src ./src
COPY alembic.ini ./
ENTRYPOINT ["python", "--version"]
